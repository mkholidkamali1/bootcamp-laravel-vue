<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        // Validasi
        $request->validate([
            'email' => 'required|email|unique:users,email',
            'name' => 'required',
            'password' => 'required|confirmed|min:6'
        ]);

        // Create User
        $user = User::create([
            'email' => $request['email'],
            'name' => $request['name'],
            'password' => Hash::make($request['password'])
        ]);

        $data['user'] = $user;

        // Response
        return response()->json([
            'response_code' => '200',
            'response_message' => 'User berhasil di register',
            'data' => $data
        ]);
    }

    public function login(Request $request)
    {
        // Validasi
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['Message' => 'Email or password invalid'], 401);
        }

        $user = Auth::user();
        return response()->json([
            'response_code' => '201',
            'response_message' => 'User berhasil di login',
            'user'  => $user,
            'token' => $token
        ], 201);
    }

    public function logout(Request $request)
    {
        auth()->logout();

        return response()->json(['message' => 'Berhasil Logout']);
    }

    public function refresh()
    {
        return response()->json([
            'status' => 'success',
            'user' => Auth::user(),
            'authorisation' => [
                'token' => Auth::refresh(),
                'type' => 'bearer',
            ]
        ]);
    }
}