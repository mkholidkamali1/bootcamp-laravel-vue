<?php

/**
 * ABILITY
 */

trait Hewan {
    public $nama;

    public $darah;

    public $jumlahKaki;

    public $keahlian;

    public function atraksi()
    {
        echo "$this->nama sedang $this->keahlian" . PHP_EOL;
    }
}

trait Fight {
    public $attackPower;

    public $defencePower;

    public function serang($musuh)
    {
        echo "$this->nama sedang menyerang $musuh->nama" . PHP_EOL;

        $musuh->diserang($this->attackPower);
    }

    public function diserang($attackPower)
    {
        echo "$this->nama sedang diserang" . PHP_EOL;

        $this->darah -= ($attackPower / $this->defencePower);
    }
}


/**
 * ENTITY 
 */

class Elang {
    use Hewan, Fight;

    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower, $darah = 50)
    {
        $this->nama         = $nama;
        $this->darah        = $darah;
        $this->jumlahKaki   = $jumlahKaki;
        $this->keahlian     = $keahlian;
        $this->attackPower  = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan()
    {
        echo "Nama          : $this->nama" . PHP_EOL;
        echo "Darah         : $this->darah" . PHP_EOL;
        echo "Jumlah Kaki   : $this->jumlahKaki" . PHP_EOL;
        echo "Keahlian      : $this->keahlian" . PHP_EOL;
        echo "Attack Power  : $this->attackPower" . PHP_EOL;
        echo "Defence Power : $this->defencePower" . PHP_EOL;
    }
}

class Harimau {
    use Hewan, Fight;

    public function __construct($nama, $jumlahKaki, $keahlian, $attackPower, $defencePower, $darah = 50)
    {
        $this->nama         = $nama;
        $this->darah        = $darah;
        $this->jumlahKaki   = $jumlahKaki;
        $this->keahlian     = $keahlian;
        $this->attackPower  = $attackPower;
        $this->defencePower = $defencePower;
    }

    public function getInfoHewan()
    {
        echo "Nama          : $this->nama" . PHP_EOL;
        echo "Darah         : $this->darah" . PHP_EOL;
        echo "Jumlah Kaki   : $this->jumlahKaki" . PHP_EOL;
        echo "Keahlian      : $this->keahlian" . PHP_EOL;
        echo "Attack Power  : $this->attackPower" . PHP_EOL;
        echo "Defence Power : $this->defencePower" . PHP_EOL;
    }
}


/**
 * BEHAVIOUR
 */

//  Init
$elang   = new Elang('Elang', '2', 'terbang tinggi', 10, 5);
$harimau = new Harimau('Harimau', '4', 'lari cepat', 7, 8);

// Action
$elang->atraksi();
$harimau->atraksi();

// Fight
$elang->serang($harimau);
$harimau->serang($elang);

// Stats
$elang->getInfoHewan();
$harimau->getInfoHewan();